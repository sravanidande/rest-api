import * as mongoose from 'mongoose';

const bookSchema = new mongoose.Schema({
  title: { type: String, required: true },
  author: { type: String, required: true }
});

export interface IBook {
  title: String;
  author: String;
}

const Book = mongoose.model<IBook>('Book', bookSchema);

export default Book;
