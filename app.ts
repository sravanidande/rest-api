import * as express from 'express';
const app = express();
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import { Request, Response } from 'express';
import Book from './book';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.connect(
  'mongodb://localhost/mydatabase',
  { useNewUrlParser: true },
  err => {
    if (err) throw new err();
    console.log('mongoose connected');
  }
);

app.get('/books', (req: Request, res: Response) => {
  try {
    const books = Book.find().exec();
    res.send(books);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.post('/books', (res: Response, req: Request) => {
  try {
    const newBook = Book.create(req.body).exec();
    const result = newBook.save();
    res.send(result);
  } catch (err) {
    res.staus(500).send(err);
  }
});

app.listen(3000, () => console.log('express listening on port 3000...'));
